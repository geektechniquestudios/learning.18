package com.geektechnique.learningswagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearningSwaggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearningSwaggerApplication.class, args);
	}

}
